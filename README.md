# Demonstracja wykorzystania framework Spring do połączenia do Kafki

1. Wykorzystany serwis zewnętrzny produkujący dane o użytkownikach systemu (User) 
   - https://random-data-api.com/api/v2/users
   - dane odczytywane przy pomocy klasy RestWebClient (implementacja użycia RestTemplate)
2. Klasa producenta (KafkaProducer) wykorzystana jest do produkowania otrzymanych informacji o użytkowniku
3. RestWebClient wykorzystuje metodę `send` producenta do wysłania wiadomości z uprzednio otrzymanymi danymi użytkownika
4. Wykorzystano `scheduler` springa - co 1 sekundę następuje wywołanie WestWebClienta a tym samy produkcja otrzymanych informacji na temat `User`a
5. Komponent konsumenta - KafkaSpringListener odczytuje dane z topica kafki i mapuje je do obiektu `User` wypisując dane 
