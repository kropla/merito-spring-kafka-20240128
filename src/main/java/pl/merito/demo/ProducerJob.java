package pl.merito.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProducerJob {

    @Autowired
    private RestWebClient restWebClient;

    @Scheduled(fixedDelay = 1000)
    public void scheduleFixedDelayTask() throws Exception {
        log.info("Fixed delay task: {}", System.currentTimeMillis() / 1000);

        restWebClient.run(new String[] {});

    }

}
