package pl.merito.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaSpringListener {

    @Autowired
    private ObjectMapper objectMapper;

    @KafkaListener(id = "id1", topicPattern = "newSpringTopic")
    public void listen(@Payload String payload, @Header(KafkaHeaders.RECEIVED_PARTITION) int partition, @Header(KafkaHeaders.OFFSET) long offset) throws JsonProcessingException {

        User user = objectMapper.readValue(payload, User.class);

        log.info(" offset: {}, partition:{}, user: {}", offset, partition, user);
    }

}
