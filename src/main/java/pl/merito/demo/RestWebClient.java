package pl.merito.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestWebClient implements CommandLineRunner {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private KafkaProducer kafkaProducer;

    @Override
    public void run(String... args) throws Exception {
        ResponseEntity<String> entity =  restTemplate.getForEntity("https://random-data-api.com/api/v2/users", String.class);

        kafkaProducer.send(entity.getBody());
    }
}
